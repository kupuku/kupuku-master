﻿
using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Answer
{
    [SerializeField] private string _quText;
    public string QuText { get { return _quText; } }

    [SerializeField] private bool _quTrue;
    public bool QuTrue { get { return _quTrue; } }

}

[CreateAssetMenu(fileName = "New Question", menuName = "Quiz/new Question")]

public class Question : ScriptableObject
{
    public enum AnswerType { Multi, Single }

    [SerializeField] private String soal = String.Empty;
    public String Soal { get { return soal; } }

    [SerializeField] Answer[] jawaban = null;
    public Answer[] Jawaban { get { return jawaban; } }

    [SerializeField] private bool _useTimer = false;
    public bool UseTimer { get { return _useTimer; } }

    [SerializeField] private int _timer = 0;
    public int Timer { get { return _timer; } }

    [SerializeField] private AnswerType _answerType = AnswerType.Multi;
    public AnswerType GetAnswerType { get { return _answerType; } }

    [SerializeField] private int _addScore = 10;
    public int AddScore { get { return _addScore; } }

    public List<int> GetCorrectAnswers ()
    {
        List<int> CorrectAnswers = new List<int>();
        for (int i = 0; i < Jawaban.Length; i++)
        {
            if (Jawaban[i].QuTrue)
            {
                CorrectAnswers.Add(i);
            }
        }
        return CorrectAnswers;
    }
}

