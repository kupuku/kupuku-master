﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Membuat fungsi agar dapat berpindah scene dengan nama QuestGame
    public void GoToQuestGame()
    {
        /*Memanggil fungsi LoadScene yg memiliki parameter bertipe string / int
         * harus kita isi dengan nama scene / index scene yang akan
         *dijadikan scene atau level selanjutnya*/
           SceneManager.LoadScene(sceneBuildIndex : 1);
    }
    //Membuat fungsi agar dapat berpindah scene dengan nama QuizGame 
    public void GoToQuizGame()
    {
        /*Memanggil fungsi LoadScene yg memiliki parameter bertipe string / int
         * harus kita isi dengan nama scene / index scene yang akan
         *dijadikan scene atau level selanjutnya*/
           SceneManager.LoadScene(sceneBuildIndex : 2);
    }
    //Membuat fungsi agar dapat berpindah scene dengan nama EduCardGame
    public void GoToEduCardGame()
    {
        /*Memanggil fungsi LoadScene yg memiliki parameter bertipe string / int
         * harus kita isi dengan nama scene / index scene yang akan
         *dijadikan scene atau level selanjutnya*/
           SceneManager.LoadScene(sceneBuildIndex : 3);
    }

    public void GoToMainMenu()
    {
        /*Memanggil fungsi LoadScene yg memiliki parameter bertipe string / int sesuai index scene
         * harus kita isi dengan nama scene / index scene yang akan
         *dijadikan scene atau level selanjutnya*/
           SceneManager.LoadScene(sceneBuildIndex : 0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
