﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable()]
public struct StateUIPar
{
    [Header("Answers Option")]

    [SerializeField] float uimargins;
    public float Margins { get { return uimargins;  } }

    [Header("Resolution Screen Options")]

    [SerializeField] Color stateTrueScreenColor;
    public Color StateTrueScreenColor { get { return stateTrueScreenColor; } }

    [SerializeField] Color stateFalseScreenColor;
    public Color StateFalseScreenColor { get { return stateFalseScreenColor; } }

    [SerializeField] Color stateFinishScreenColor;
    public Color StateFinishScreenColor { get { return stateFinishScreenColor; } }

}

[Serializable()]
public struct GameUI
{
    [SerializeField] RectTransform ansContentRect;
    public RectTransform AnsContentRect { get { return ansContentRect; } }

    [SerializeField] TextMeshProUGUI quText;
    public TextMeshProUGUI QuText { get { return quText; } }

    [SerializeField] TextMeshProUGUI skorText;
    public TextMeshProUGUI SkorText { get { return skorText; } }

    [Space]

    [SerializeField] Animator gamestateAnimation;
    public Animator GameStateAnimation { get { return gamestateAnimation;  } }

    [SerializeField] Image resBackground;
    public Image ResBackground { get { return resBackground; } }

    [SerializeField] TextMeshProUGUI stateResText;
    public TextMeshProUGUI StateResText { get { return stateResText; } }

    [SerializeField] TextMeshProUGUI skorResText;
    public TextMeshProUGUI SkorResText { get { return skorResText;  } }

    [Space]

    [SerializeField] TextMeshProUGUI recordText;
    public TextMeshProUGUI RecordText {  get { return recordText; } }

    [SerializeField] CanvasGroup gameMainCanvas;
    public CanvasGroup GameMainCanvas { get { return gameMainCanvas; } }

    [SerializeField] RectTransform rectUIGameOver;
    public RectTransform RectUIGameOver { get { return rectUIGameOver; } }
}

public class UIManager : MonoBehaviour {

    public enum TypeOfStateScreen { True, False, GameOver }

    [Header("References")]
    [SerializeField] GameEvents gameEvents;

    [Header("UI Elements (Prefabs)")]
    [SerializeField] AnswerData quizAnsPrefab;

    [SerializeField] GameUI gameUI;

    [Space]

    [SerializeField] StateUIPar parameters;

    List<AnswerData> recentAns = new List<AnswerData>();
    private List<int> doneQu = new List<int>();
    private int resStateParaHash = 0;

    private IEnumerator ShowTimedRes_IE;

    void OnEnable()
    {
        gameEvents.UpdateQuestionUI += QuUpdate;
        gameEvents.DisplayResolutionScreen += ShowReso;
        gameEvents.ScoreUpdated += ScoreUpdate;
    }

    void OnDisable()
    {
        gameEvents.UpdateQuestionUI -= QuUpdate;
        gameEvents.DisplayResolutionScreen -= ShowReso;
        gameEvents.ScoreUpdated -= ScoreUpdate;
    }

    void Start ()
    {
        ScoreUpdate ();
        resStateParaHash = Animator.StringToHash("ScreenState");
    }

    void QuUpdate(Question qu)
    {
        gameUI.QuText.text = qu.Soal;
        CreateAns(qu);
    }
    
    void ShowReso (TypeOfStateScreen type, int score)
    {
        ResScreenUIUpdate(type, score);
        gameUI.GameStateAnimation.SetInteger(resStateParaHash, 2);
        gameUI.GameMainCanvas.blocksRaycasts = false;

        if (type != TypeOfStateScreen.GameOver)
        {
            if (ShowTimedRes_IE != null)
            {
                StopCoroutine(ShowTimedRes_IE);
            }
            ShowTimedRes_IE = ShowTimedRes();
            StartCoroutine(ShowTimedRes_IE);
        }
    }


    IEnumerator ShowTimedRes ()
    {
        yield return new WaitForSeconds(GameUtility.ResolutionDelayTime);
        gameUI.GameStateAnimation.SetInteger(resStateParaHash, 1);
        gameUI.GameMainCanvas.blocksRaycasts = true;
    }


    void ResScreenUIUpdate (TypeOfStateScreen type, int score)
    {
        var record = PlayerPrefs.GetInt(GameUtility.SavePrefKey);

        switch (type)
        {
            case TypeOfStateScreen.True:
                gameUI.ResBackground.color = parameters.StateTrueScreenColor;
                gameUI.StateResText.text = "BENAR !";
                gameUI.SkorResText.text = "+" + score;
                break;

            case TypeOfStateScreen.False:
                gameUI.ResBackground.color = parameters.StateFalseScreenColor;
                gameUI.StateResText.text = "SALAH.";
                gameUI.SkorResText.text = "-" + score;
                break;

            case TypeOfStateScreen.GameOver:
                gameUI.ResBackground.color = parameters.StateFinishScreenColor;
                gameUI.StateResText.text = "SKOR AKHIRMU :";

                StartCoroutine(CountScore_IE());
                gameUI.RectUIGameOver.gameObject.SetActive(true);
                gameUI.RecordText.gameObject.SetActive(true);
                gameUI.RecordText.text = ((record > gameEvents.StartupHighScore) ? "<color=yellow>new </color>" : string.Empty) + "REKOR SKOR : " + record;
                break;
        }
    }

    IEnumerator CountScore_IE()
    {
        if(gameEvents.CurrentFinalScore == 0) { gameUI.SkorResText.text = 0.ToString(); }

        var recentScoreUI = 0;
        var aboveZero = gameEvents.CurrentFinalScore > 0;
        while (recentScoreUI < gameEvents.CurrentFinalScore)
        {
            recentScoreUI += aboveZero ? 1 : -1;
            gameUI.SkorResText.text = recentScoreUI.ToString();
            yield return null;
        }
    }

    void CreateAns(Question qu)
    {
        ClearAns();

        float offset = 0 - parameters.Margins;
        for (int i = 0; i < qu.Jawaban.Length; i++) 
        {
            AnswerData newAnswer = (AnswerData)Instantiate(quizAnsPrefab, gameUI.AnsContentRect);
            newAnswer.UpdateData(qu.Jawaban[i].QuText, i);

            newAnswer.Rect.anchoredPosition = new Vector2(0, offset);

            offset -= (newAnswer.Rect.sizeDelta.y + parameters.Margins);
            gameUI.AnsContentRect.sizeDelta = new Vector2(gameUI.AnsContentRect.sizeDelta.x, offset * -1);

            recentAns.Add(newAnswer);
        }
    }

    void ClearAns()
    {
        foreach (var answer in recentAns)
        {
            Destroy(answer.gameObject);
        }
        recentAns.Clear();
    }

    void ScoreUpdate()
    {
        gameUI.SkorText.text = "Poin : " + gameEvents.CurrentFinalScore;
    }
}
