﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    Question[] _qu = null;
    public Question[] Qu { get { return _qu; } }

    [SerializeField] GameEvents gameEvents = null;

    [SerializeField] Animator animatorTimer = null;
    [SerializeField] TextMeshProUGUI teksTimer = null;
    [SerializeField] Color warnaTimerSetengahAbis = Color.green;
    [SerializeField] Color warnaTimerAbis = Color.red;
    private Color warnaTimerDefault = Color.blue;

    private List<AnswerData> ansKepilih = new List<AnswerData>();
    private List<int> QuFinished = new List<int>();
    private int QuRecent = 0;

    private int hashTimerStateParameter = 0;

    private IEnumerator enumNextTurn_IE = null;
    private IEnumerator enumMulaiTimer_IE;


    private bool GameFinish
    {
        get
        {
            return (QuFinished.Count < 6) ? false : true;
        }
    }

    void OnEnable()
    {
        gameEvents.UpdateQuestionAnswer += UpdateAns; 
    }

    void OnDisable()
    {
        gameEvents.UpdateQuestionAnswer -= UpdateAns;
    }

    void Awake()
    {
        gameEvents.CurrentFinalScore = 0;
    }

    void Start()
    {
        gameEvents.StartupHighScore = PlayerPrefs.GetInt(GameUtility.SavePrefKey);

        warnaTimerDefault = teksTimer.color;

        StoreQu();

        hashTimerStateParameter = Animator.StringToHash("TimerState");

        var seed = Random.Range(int.MinValue, int.MaxValue);
        Random.InitState(seed);

        Display();
    }

    public void UpdateAns (AnswerData newAnswer)
    {
        if (Qu[QuRecent].GetAnswerType == Question.AnswerType.Single)
        {
            foreach (var answer in ansKepilih)
            {
                if (answer != newAnswer)
                {
                    answer.Reset();
                }
            }
            ansKepilih.Clear();
            ansKepilih.Add(newAnswer);
        }

        else
        {
            bool alrPickedAns = ansKepilih.Exists(X => X == newAnswer);
            if (alrPickedAns)
            {
                ansKepilih.Remove(newAnswer);
            }
            else
            {
                ansKepilih.Add(newAnswer);
            }
        }   
    }

    public void DeleteAns()
    {
        ansKepilih = new List<AnswerData>();
    }

    void Display()
    {
        DeleteAns();
        var qu = GetRandQu();

        if (gameEvents.UpdateQuestionUI == null)
        { Debug.LogWarning("Ups! ada yang salah pas ingin coba ngemunculin new Question UI Data. GameEvents.UpdateQuestionUI is null.  masalah terjadi pada : GameManager.Display() method."); } 
        else
        {
            gameEvents.UpdateQuestionUI(qu);
        }

        if (gameEvents.UpdateQuestionUI != null)
        {
            UpdTimer(qu.UseTimer);
        }
    }

    public void Accept ()
    {
        UpdTimer(false); 
        bool isCorrect = CheckUserAns();
        QuFinished.Add(QuRecent);

        UpdScore((isCorrect) ? Qu[QuRecent].AddScore : -Qu[QuRecent].AddScore);
        if (GameFinish)
        {
            SetScoreRecord();
        }

        var type = (GameFinish) ? UIManager.TypeOfStateScreen.GameOver : (isCorrect) ? UIManager.TypeOfStateScreen.True : UIManager.TypeOfStateScreen.False;

        if (gameEvents.DisplayResolutionScreen != null)
        {
            gameEvents.DisplayResolutionScreen(type, Qu[QuRecent].AddScore);
        }

        AudioManager.Instance.PlaySound((isCorrect) ? "CorrectSFX" : "IncorrectSFX");

        if (type != UIManager.TypeOfStateScreen.GameOver)
        {
            if (enumNextTurn_IE != null)
            {
                StopCoroutine(enumNextTurn_IE);
            }
            enumNextTurn_IE = WaitForRoundEnd();
            StartCoroutine(enumNextTurn_IE);
        }
       
    }

    void UpdTimer(bool state)
    {
        switch (state)
        {
            case true:
                enumMulaiTimer_IE = InitiateGameTimer();
                StartCoroutine(enumMulaiTimer_IE);

                animatorTimer.SetInteger(hashTimerStateParameter, 2);
                break;

            case false:
                if (enumMulaiTimer_IE != null)
                {
                    StopCoroutine(enumMulaiTimer_IE);
                }

                animatorTimer.SetInteger(hashTimerStateParameter, 1);
                break;
        }
    }


    IEnumerator InitiateGameTimer ()
    {
        var totalWaktu = Qu[QuRecent].Timer;
        var sisaWaktu = totalWaktu;

        teksTimer.color = warnaTimerDefault;
        while (sisaWaktu > 0)
        {
            sisaWaktu--;

            AudioManager.Instance.PlaySound("CountdownSFX");

            if (sisaWaktu < totalWaktu / 2 && sisaWaktu > totalWaktu / 3)
            {
                teksTimer.color = warnaTimerSetengahAbis;
            }
            if (sisaWaktu < totalWaktu / 3)
            {
                teksTimer.color = warnaTimerAbis;
            }

            teksTimer.text = sisaWaktu.ToString();
            yield return new WaitForSeconds(1.0f);
        }
        Accept();
    }

    IEnumerator WaitForRoundEnd()
    {
        yield return new WaitForSeconds(GameUtility.ResolutionDelayTime);
        Display();
    }

    Question GetRandQu()
    {
        var randIndex = GetRandomIndexQu();
        QuRecent = randIndex;

        return Qu[QuRecent];
    }

    int GetRandomIndexQu()
    {
        var random = 0;
        if (QuFinished.Count < Qu.Length)
        {
            do
            {
                random = UnityEngine.Random.Range(0, Qu.Length);
            } while (QuFinished.Contains(random) || random == QuRecent);
        }
        return random;
    }

    bool CheckUserAns()
    {
        return !CompareGameAns() ? false : true;
    }

    bool CompareGameAns ()
    {
        if (ansKepilih.Count > 0)
        {
            List<int> c = Qu[QuRecent].GetCorrectAnswers();
            List<int> p = ansKepilih.Select(x => x.AnswerIndex).ToList();

            var f = c.Except(p).ToList();
            var s = p.Except(c).ToList();

            return !f.Any() && !s.Any();
        }
        return false;
    }

    public void PlayAgain ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void BackToMenu ()
    {
        SceneManager.LoadScene(sceneBuildIndex : 0);
    }

    void StoreQu ()
    {
        Object[] objects = Resources.LoadAll("Questions", typeof (Question));
        _qu = new Question[objects.Length];
        for (int i = 0; i < objects.Length; i++)
        {
            _qu[i] = (Question)objects[i];
        }
    }

    private void SetScoreRecord ()
    {
        var scorerecord = PlayerPrefs.GetInt(GameUtility.SavePrefKey);
        if (scorerecord < gameEvents.CurrentFinalScore)
        {
            PlayerPrefs.SetInt(GameUtility.SavePrefKey, gameEvents.CurrentFinalScore);
        }
    }

    private void UpdScore (int add)
    {
        gameEvents.CurrentFinalScore += add;
        if (gameEvents.CurrentFinalScore < 0)
        {
            gameEvents.CurrentFinalScore = 0;
        }
        gameEvents.ScoreUpdated?.Invoke();
    }
}
