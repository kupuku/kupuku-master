﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public string[] trackedCards;

    public PlayerData (DefaultTrackableEventHandler player)
    {
        trackedCards = player.trackedCardsCollection;
    }
}
