﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayManager : MonoBehaviour
{
    public GameObject panel;

    public void showPanel()
    {
        panel.SetActive(true);
    }

    public void hidePanel() => panel.SetActive(false);
}
