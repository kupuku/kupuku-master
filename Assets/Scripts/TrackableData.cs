﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.Linq;
using UnityEngine.UI;

public class TrackableData : MonoBehaviour
{
    public List<string> trackedCards;
    public string[] trackedCardsCollection;
   

    void Start()
    {
        Load();
    }
    void Update()
    {

        
    }

    public void Save()
    {
        //SaveSystem.Save(this);
    }

    public void Load()
    {
        PlayerData data = SaveSystem.Load();
        trackedCards = data.trackedCards.ToList();
    }
}
