﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;

public class EducardMenu : MonoBehaviour
{
    public string[] trackedCard;
    public Text trackedCardCount;
    public GameObject Slide1, Slide2;
    public GameObject[] cardimages;
    public Image boardimage;
    public bool[] cardState;
    public Text title;
    public Text description;
    public GameObject board;


    void Start()
    {
        board.SetActive(false);
        load();
        Slide2.SetActive(false);
        // set the desired aspect ratio (the values in this example are
        // hard-coded for 16:9, but you could make them into public
        // variables instead so you can set them at design time)
        float targetaspect = 16.0f / 9.0f;

        // determine the game window's current aspect ratio
        float windowaspect = (float)Screen.width / (float)Screen.height;

        // current viewport height should be scaled by this amount
        float scaleheight = windowaspect / targetaspect;

        // obtain camera component so we can modify its viewport
        Camera camera = GetComponent<Camera>();

        // if scaled height is less than current height, add letterbox
        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            camera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            camera.rect = rect;
        }
    }

    

    public void onClickRightArrow()
    {
        if (Slide1.activeSelf == true)
        {
            Slide1.SetActive(false);
            Slide2.SetActive(true);
        } else if (Slide2.activeSelf == true)
        {
            Slide2.SetActive(false);
            Slide1.SetActive(true);
        }
    }

    public void onClickLeftArrow()
    {
        if (Slide1.activeSelf == true)
        {
            Slide1.SetActive(false);
            Slide2.SetActive(true);
        }
        else if (Slide2.activeSelf == true)
        {
            Slide2.SetActive(false);
            Slide1.SetActive(true);
        }
    }


    public void openARCamera()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void closeARCamera()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void backToMainMenu()
    {
        SceneManager.LoadScene(sceneBuildIndex : 0);
    }

    public void closePanel ()
    {
        board.SetActive(false);
    }

    public void openPanel(int index)
    {
        boardimage.transform.GetComponent<Image>().sprite = cardimages[index].GetComponent<Image>().sprite;
        title.text = Resources.Load<TextAsset>("Cards/Name/" + cardimages[index].GetComponent<Image>().sprite.name).text;
        description.text = Resources.Load<TextAsset>("Cards/Description/" + cardimages[index].GetComponent<Image>().sprite.name).text;
        //string txttitlestring = Encoding.ASCII.GetString(txttitle.bytes);
        //string txtdescstring = Encoding.ASCII.GetString(txtdesc.bytes);
        //title.text = txttitlestring;
        //description.text = txtdescstring;
        board.SetActive(true);
    }

    public void load()
    {
        PlayerData data = SaveSystem.Load();
        trackedCardCount.text = data.trackedCards.Length.ToString();
        for (int i = 0; i < data.trackedCards.Length; i++)
        {
            cardimages[i].transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Cards/Image/" + data.trackedCards[i]);
            int closureIndex = i;
            cardimages[closureIndex].GetComponent<Button>().onClick.AddListener(() => openPanel(closureIndex));
            
        }
        
    }
}
